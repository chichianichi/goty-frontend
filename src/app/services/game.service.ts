import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

import { of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { Game } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  public games: Game[] = [];

  constructor(private http: HttpClient) {}

  getNominees() {
    if (this.games.length === 0) {
      return this.http
        .get<Game[]>(`${environment.url}/api/goty`)
        .pipe(tap((games) => (this.games = games)));
    } else {
      return of(this.games);
    }
  }

  giveVoteToGame(id: string) {
    return this.http.post(`${environment.url}/api/goty/${id}`, {}).pipe(
      catchError((err) => {
        console.log('Request error');
        return of(err.error);
      })
    );
  }
}
