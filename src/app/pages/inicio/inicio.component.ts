import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

import { map } from 'rxjs/operators';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
})
export class InicioComponent implements OnInit {
  public games!: any[];

  constructor(private db: AngularFirestore) {}

  ngOnInit(): void {
    //esto es un Observable
    this.db
      .collection('goty')
      .valueChanges()
      .pipe(
        map((resp: any[]) => {
          return resp.map(({ name, votes }) => ({ name, data: [votes] }));
        })
      )
      .subscribe((resp) => {
        this.games = resp;
      });
  }
}
