import {
  Component,
  ViewChild,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexLegend,
  ApexPlotOptions,
  ApexTheme,
  ApexYAxis,
  ApexTooltip,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  tooltip: ApexTooltip;
  plotOptions: ApexPlotOptions;
  theme: ApexTheme;
  legend: ApexLegend;
};

@Component({
  selector: 'app-horizontal-bar-chart',
  templateUrl: './horizontal-bar-chart.component.html',
  styleUrls: ['./horizontal-bar-chart.component.css'],
})
export class HorizontalBarChartComponent implements OnChanges {
  @Input() results: any[] = [];
  @ViewChild('chart') chart!: ChartComponent;
  public chartOptions: Partial<ChartOptions> = {
    series: [{ data: [], name: '' }],
    chart: {
      type: 'bar',
      height: 300,
      toolbar: {
        show: false,
      },
    },
    plotOptions: {
      bar: {
        horizontal: true,
      },
    },
    xaxis: {
      categories: [''],
      labels: {
        style: {
          colors: ['#FFF'],
        },
      },
    },
    yaxis: {
      title: {
        text: 'Votes',
        style: {
          color: '#FFF',
        },
      },
      labels: {
        style: {
          colors: ['#FFF'],
        },
      },
    },
    theme: {
      palette: 'palette8',
    },
    tooltip: {
      enabled: false,
    },
    legend: {
      labels: {
        colors: ['#FFF'],
      },
    },
  };

  values = [[10], [4], [35], [20]];
  interval: any;

  ngOnChanges(changes: SimpleChanges): void {
    this.chartOptions.series = this.results;
  }

  constructor() {}
}
