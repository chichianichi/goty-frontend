import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgApexchartsModule } from 'ng-apexcharts';

import { NavbarComponent } from './navbar/navbar.component';
import { HorizontalBarChartComponent } from './horizontal-bar-chart/horizontal-bar-chart.component';

@NgModule({
  declarations: [NavbarComponent, HorizontalBarChartComponent],
  exports: [NavbarComponent, HorizontalBarChartComponent],
  imports: [CommonModule, RouterModule, NgApexchartsModule],
})
export class ComponentsModule {}
