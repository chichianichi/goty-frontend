// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: 'http://localhost:5000/firestore-chart-goty/us-central1',
  firebase: {
    apiKey: 'AIzaSyBAncs3lTJyzB9b89qJh4gXmyPbaeePlns',
    authDomain: 'firestore-chart-goty.firebaseapp.com',
    projectId: 'firestore-chart-goty',
    storageBucket: 'firestore-chart-goty.appspot.com',
    messagingSenderId: '831329726737',
    appId: '1:831329726737:web:19a075a5f6a68cfe0edcb0',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
